
# OH WEEKLY ASSESSMENT 2

  

  

In this assessment, you have to answer some basic question about Internet. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.

  

Deadline: Saturday, 28 December 2021, 23:59 WIB


## Instructions
- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-2` as the name of project
- Push your result to it
- Fill this form when you are finish: https://docs.google.com/forms/d/e/1FAIpQLSfe-kDIu-efXh_62tq3dAl3DwuN9PB2Pae0ywYPKSMUiM0sOw/viewform


## Task Requirements
- [-] Complete: Answer all Question
  
  

Question:

- Explain How the Internet Works.

Internet adalah kumpulan komputer yang tergabung dalam jaringan skala luas. 

- What's the meaning of communication protocol

Protokol komunikasi adalah ketentuan yang berlaku dalam pertukaran data antara 2 atau lebih komputer dalam jaringan skala lokal maupun luas.  

- Mention HTTP status codes

Informational responses (100–199)
Successful responses (200–299)
Redirection messages (300–399)
Client error responses (400–499)
Server error responses (50

- Make Analogy Regarding how Front end and Backend communicate each other

Seperti dalam sebuah restoran, front end sebagai waiter yang menyajikan menu yang dipesan pelanggan. Dimana menu menu tersebut sudah ditetapkan sebelumnya dan back end adalah koki yang membuat setiap pesanan dari customer. 

- Mention What method in HTTP protocol

Get 
Head 
Post 
Put
Delet
  
  ## Please answer this question in markdown format